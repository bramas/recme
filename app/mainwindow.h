#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSettings>
#include <QTimer>
#include <QProcess>
#include <QPushButton>
#include <QTimer>

#include <openssl/rsa.h>
#include <openssl/pem.h>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QWidget
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void restaureCurrentConfig();
    void startFromCm();

public slots:
    void buttonQuit_click();
    void buttonStart_click();
    void buttonStop_click();
    void butttonOutputDir_click();
    void comboConfig_change();
    void configAdd_click();
    void configDel_click();
    void buttonLoadKey_click();
    void saveCurrentConfigDetails();
    void buttonSaveConfig_click();
    void buttonLoadConfig_click();
    void captureEventUser();
    void captureEventAuto();
    void pubkeyPath_edited();

private:
    QVector<QPixmap>  shootScreens() const;
    void  saveShots(const QVector<QPixmap>& shots);
    std::pair<double,QVector<QRect>> computeDiff(const QVector<QPixmap>& shots) const;
    QVector<QPixmap>  compactDiff(const QVector<QPixmap>& shots, const QVector<QRect>& diffrects) const;

    QSettings settings;
    QPushButton* stopButton;

    Ui::MainWindow *ui;

    bool writeContent(const QString& targetFile, std::vector<unsigned char> content);
    void printError(const char* label);
    void cleanptr();
    std::vector<unsigned char> out;

    RSA* rsa;
    EVP_PKEY *public_evp_key;    
    EVP_PKEY_CTX *ctx;

    QTimer captureTimerUser;
    QTimer captureTimerAuto;
    QVector<QPixmap> latestShots;
    QString listOfFiles;
    QString targetDir;
};
#endif // MAINWINDOW_H

