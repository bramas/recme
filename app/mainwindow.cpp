#include "mainwindow.h"
#include "./ui_mainwindow.h"
#include "foldercompressor.hpp"

#include <QFileDialog>
#include <QMessageBox>
#include <QString>
#include <QDir>
#include <QInputDialog>
#include <QDebug>
#include <QTemporaryFile>
#include <QScrollBar>
#include <QHash>
#include <QRegularExpression>
#include <QPoint>
#include <QGraphicsTextItem>
#include <cstdlib>
#include <QGuiApplication>
#include <QScreen>
#include <QDebug>
#include <QDesktopWidget>
#include <QDate>
#include <QTime>
#include <QBuffer>

#include <iostream>
#include <fstream>

#include <openssl/rsa.h>
#include <openssl/pem.h>
#include <openssl/err.h>

MainWindow::MainWindow(QWidget *parent)
    : QWidget(parent), settings("UNISTRA", "RECME"),
      ui(new Ui::MainWindow),
      rsa(NULL),
      public_evp_key(NULL),
      ctx(NULL)
{
    ui->setupUi(this);

    // QApplication::desktop()->screenGeometry(); QDesktopWidget
    // QScreen QGuiApplication::primaryScreen()->geometry();
    //const QRect screenSize = QApplication::desktop()->screenGeometry();
    QDesktopWidget* widget = QApplication::desktop();//qApp->desktop();
    QRect screenSize = widget->availableGeometry(widget->primaryScreen());
    this->setGeometry(screenSize.width()/2-width()/2, screenSize.height()/2-height()/2, width(), height());
    this->setMinimumSize(QSize(width(),height()));
    this->setMaximumSize(QSize(width()*1.5,height()*1.5));

    this->stopButton = new QPushButton(this);
    this->stopButton->setVisible(false);
    this->stopButton->setText("Stop Recme");
    this->stopButton->setWindowFlags(Qt::WindowStaysOnTopHint | Qt::Window | Qt::FramelessWindowHint);
    this->stopButton->setWindowOpacity(0.3);
    this->stopButton->setGeometry(20,20, 100, 30);
    QObject::connect(this->stopButton, &QPushButton::clicked,
                     this, &MainWindow::buttonStop_click);


    const QStringList allConfigs = settings.value("all-config", QStringList() << "Default").value<QStringList>();
    const int currentConfigIdx = settings.value("current-config-idx", 0).toInt();
    for(int idx = 0 ; idx < allConfigs.size() ; ++idx){
        ui->profileCombo->addItem(allConfigs[idx]);
    }
    qDebug() << "Config index from setting " << currentConfigIdx << " there are " << ui->profileCombo->count() << " configs\n";
    ui->profileCombo->setCurrentIndex(currentConfigIdx < ui->profileCombo->count() ? currentConfigIdx : 0);

    QObject::connect(&captureTimerUser, &QTimer::timeout,
                     this, &MainWindow::captureEventUser);
    captureTimerAuto.setInterval(30*1000);
    QObject::connect(&captureTimerAuto, &QTimer::timeout,
                     this, &MainWindow::captureEventAuto);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::buttonQuit_click(){
    this->close();
}

void MainWindow::startFromCm(){
    QMetaObject::invokeMethod(ui->buttonStart, "clicked");
}

// https://www.programmersought.com/article/37955188510/
// https://www.openssl.org/docs/man1.0.2/man3/EVP_PKEY_encrypt.html
void MainWindow::buttonStart_click(){
    if(!QDir(ui->buttonOutputDir->text()).exists()){
        QMessageBox msgBox;
        msgBox.setText("Directory " + ui->buttonOutputDir->text() + " does not exist.");
        msgBox.exec();
        return;
    }
    if(ui->pubkeyPath->text().size() != 0 && !QFile(ui->pubkeyPath->text()).exists()){
        QMessageBox msgBox;
        msgBox.setText("File " + ui->pubkeyPath->text() + " does not exist.");
        msgBox.exec();
        return;
    }
    targetDir = ui->buttonOutputDir->text() + "/recme-" + QDate::currentDate().toString("yyyy-MM-dd") + "_" + QTime::currentTime().toString("hh-mm-ss-zzz");
    QDir targetDirCore(targetDir);
    if(targetDirCore.exists() == false){
        if(targetDirCore.mkpath(targetDir) == false){
            QMessageBox msgBox;
            msgBox.setText("Cannot create " + targetDir + ".");
            msgBox.exec();
            return;
        }
    }

    this->rsa = NULL;
    this->public_evp_key = NULL;
    this->ctx = NULL;

    if(ui->pubkeyPath->text().size()){
        FILE *f;
        f = fopen(ui->pubkeyPath->text().toStdString().c_str(), "r");
        if(f == nullptr){
            QMessageBox msgBox;
            msgBox.setText("Cannot open key file " + ui->pubkeyPath->text() + " .");
            msgBox.exec();
            return;
        }

        rsa = RSA_new();
        if( PEM_read_RSAPublicKey(
                    f,     /* use the FILE* that was opened */
                    &rsa, /* pointer to EVP_PKEY structure */
                    NULL,  /* password callback - can be NULL */
                    NULL   /* parameter passed to callback or password if callback is NULL */
                    ) == nullptr){
            fclose(f);
            printError("PEM_read_RSAPublicKey");
            return ;
        }
        fclose(f);

        public_evp_key = EVP_PKEY_new();
        EVP_PKEY_assign_RSA(public_evp_key, rsa);

        ctx = EVP_PKEY_CTX_new(public_evp_key, NULL);
        if (ctx == NULL) {
            printError("EVP_PKEY_CTX_new");
            return;
        }
        if (EVP_PKEY_encrypt_init(ctx) < 0) {
            printError("EVP_PKEY_encrypt_init");
            return;
        }
        if (EVP_PKEY_CTX_set_rsa_padding(ctx, RSA_PKCS1_OAEP_PADDING/*EVP_PADDING_PKCS7*/) != 1) {
            printError("EVP_PKEY_CTX_set_rsa_padding");
            return;
        }
    }
    else{
        //QMessageBox msgBox;
        //msgBox.setText("No key given, the pictures will not be crypted.");
        //msgBox.exec();
    }

    this->stopButton->setVisible(true);
    this->setVisible(false);

    if(ui->checkPictures->isChecked()){
        captureTimerUser.setInterval(ui->picturesFreq->value()*1000);
        captureTimerUser.start();
    }
    if(ui->checkChanges->isChecked()){
        captureTimerAuto.start();
    }
    latestShots = shootScreens();
    listOfFiles.clear();
}

void MainWindow::printError(const char* label){
    unsigned long err = ERR_get_error(); //Get the error number
    char err_msg[1024] = { 0 };
    ERR_error_string(err, err_msg);
    QMessageBox msgBox;
    msgBox.setText(QString(label) + " failed: " + QString(err_msg) + " " + QString::number(err) + ".");
    msgBox.exec();
    cleanptr();
}

void MainWindow::cleanptr(){
    //if(rsa != NULL)
    //    RSA_free(rsa);
    if (public_evp_key != NULL){
        EVP_PKEY_free(public_evp_key);
        public_evp_key = NULL;
    }
    if (ctx != NULL){
        EVP_PKEY_CTX_free(ctx);
        ctx = NULL;
    }
}

void MainWindow::buttonStop_click(){
    captureTimerUser.stop();
    captureTimerAuto.stop();
    this->stopButton->setVisible(false);
    this->setVisible(true);

    const QString basename = "list-of-files_" + QTime::currentTime().toString("hh-mm-ss-zzz");
    const QString outputfilename = targetDir + "/" + basename + ".txt.recme";
    qDebug() << "   - Will list of file " << outputfilename;
    if(ui->pubkeyPath->text().size()){
        QByteArray utf8BAString1 = listOfFiles.toUtf8();
        std::vector<unsigned char> data(utf8BAString1.data(), utf8BAString1.data()+utf8BAString1.size());
        writeContent(outputfilename, data);
    }
    else{
        std::ofstream outputfile(outputfilename.toStdString());
        listOfFiles.append("Will write ").append(outputfilename).append("\n");
        if(outputfile.is_open()){
            outputfile << listOfFiles.toStdString() << std::endl;
        }
        else{
            QMessageBox msgBox;
            msgBox.setText("Cannot open " + outputfilename);
            msgBox.exec();
        }
    }
    cleanptr();

    if(FolderCompressor().compressFolder(targetDir, targetDir + ".comp") == false){
        QMessageBox msgBox;
        msgBox.setText("Failed to compress " + targetDir);
        msgBox.exec();
    }
}

std::pair<double,QVector<QRect>> MainWindow::computeDiff(const QVector<QPixmap>& shots) const{
    QVector<QRect> diffrects;
    double score = 0;
    for(int idxScreen = 0 ; idxScreen < shots.count() ; ++idxScreen){
        if(shots[idxScreen].rect() != latestShots[idxScreen].rect()){
            score += 100./shots.count();
            diffrects.append(shots[idxScreen].rect());
        }
        else{
            int xmin = std::numeric_limits<int>::max();
            int ymin = std::numeric_limits<int>::max();
            int xmax = std::numeric_limits<int>::min();
            int ymax = std::numeric_limits<int>::min();
            const QImage img = shots[idxScreen].toImage();
            const QImage latestImg = latestShots[idxScreen].toImage();
            for (int idxWidth = 0; idxWidth < img.width(); ++idxWidth ) {
                for (int idxHeight = 0; idxHeight < img.height() ; ++idxHeight){
                    const QRgb pixel = img.pixel(idxWidth,idxHeight);
                    const QRgb latestPixel = latestImg.pixel(idxWidth,idxHeight);
                    if(pixel != latestPixel){
                        score += (100./shots.count()) * 1. / (img.width()*img.height());
                        xmin = std::min(xmin, idxWidth);
                        ymin = std::min(ymin, idxWidth);
                        xmax = std::max(xmax, idxHeight);
                        ymax = std::max(ymax, idxHeight);
                    }
                }
            }
            diffrects.append(QRect(xmin, ymin, xmax-xmin+1, ymax-ymin+1));
        }
    }
    return std::make_pair(score, diffrects);
}

QVector<QPixmap>  MainWindow::compactDiff(const QVector<QPixmap>& shots, const QVector<QRect>& diffrects) const{
    QVector<QPixmap> compacted;
    for(int idxScreen = 0 ; idxScreen < shots.count() ; ++idxScreen){
        compacted.append(shots[idxScreen].copy(diffrects[idxScreen]));
    }
    return compacted;
}


QVector<QPixmap>  MainWindow::shootScreens() const{
    QVector<QPixmap> shots;

    const auto screens = qApp->screens();
    qDebug() << "- Number of screen: " << screens.count();
    for (int idxScreen = 0; idxScreen < screens.count(); ++idxScreen) {
        qDebug() << "  - screen " << idxScreen << " size:" << screens[idxScreen]->geometry();
        shots.append(screens[idxScreen]->grabWindow(0));
    }
    return  shots;
}


void  MainWindow::saveShots(const QVector<QPixmap>& shots){
    const QString basename = "shot_" + QTime::currentTime().toString("hh-mm-ss-zzz");
    for(int idxScreen = 0 ; idxScreen < shots.count() ; ++idxScreen){
        if(ui->pubkeyPath->text().size()){
            QByteArray bytes;
            QBuffer buffer(&bytes);
            buffer.open(QIODevice::WriteOnly);
            if(shots[idxScreen].save(&buffer, "JPG") == false){
                QMessageBox msgBox;
                msgBox.setText("Cannot convert image...");
                msgBox.exec();
            }
            else{
                const QString outputfilename = targetDir + "/" + basename + "screen-" + QString::number(idxScreen) + ".jpg.recme";
                qDebug() << "   - Will save image " << outputfilename;
                std::vector<unsigned char> data(bytes.begin(), bytes.end());
                writeContent(outputfilename, data);
            }
        }
        else{
            const QString outputfilename = targetDir + "/" + basename + "screen-" + QString::number(idxScreen) + ".jpg";
            qDebug() << "   - Save image " << outputfilename;
            listOfFiles.append("Will write ").append(outputfilename).append("\n");
            if(shots[idxScreen].save(outputfilename) == false){
                QMessageBox msgBox;
                msgBox.setText("Cannot save image: " + outputfilename);
                msgBox.exec();
            }
        }
    }
}

void MainWindow::captureEventUser(){
    qDebug() << "# captureEvent";
    QVector<QPixmap> shots = shootScreens();

    saveShots(shots);

    latestShots = shots;
}

void MainWindow::captureEventAuto(){
    qDebug() << "# captureEventAuto";
    QVector<QPixmap> shots = shootScreens();
    const std::pair<double,QVector<QRect>> diffScore = computeDiff(shots);
    qDebug() << "  - diffScore = " << diffScore.first;
    if(diffScore.first >= ui->changesPerct->value()){
        if(ui->checkCompact->isChecked()){
            saveShots(compactDiff(shots, diffScore.second));
        }
        else{
            saveShots(shots);
        }
    }
    latestShots = shots;
}

bool MainWindow::writeContent(const QString& targetFile, std::vector<unsigned char> content){
    listOfFiles.append("Will write ").append(targetFile).append("\n");
    if(public_evp_key == NULL || ctx == NULL){
        listOfFiles.append("Problem with public_evp_key == NULL || ctx == NULL\n");
        QMessageBox msgBox;
        msgBox.setText("OpenSSL badly initialized...");
        msgBox.exec();
        return false;
    }

    std::ofstream outputfile(targetFile.toStdString(), std::ios::binary);
    if(outputfile.is_open()){
        const size_t maxRsaLen = 64;//RSA_size(rsa)-1;
        const size_t nbParts = (content.size()+maxRsaLen-1)/maxRsaLen;
        if(content.size()){
            for(size_t idxData = 0 ; idxData < content.size() ; idxData += maxRsaLen){
                size_t currentBlockSize = std::min(content.size()-idxData, maxRsaLen);
                size_t outlen = 0;
                if (EVP_PKEY_encrypt(ctx, NULL, &outlen, &content[idxData], currentBlockSize) <= 0){
                    listOfFiles.append("- EVP_PKEY_encrypt - get size\n");
                    printError("EVP_PKEY_encrypt - get size");
                    return false;
                }

                out.resize(outlen);

                if (EVP_PKEY_encrypt(ctx, out.data(), &outlen, &content[idxData], currentBlockSize) <= 0){
                    listOfFiles.append("- EVP_PKEY_encrypt - encrypt\n");
                    printError("EVP_PKEY_encrypt - encrypt");
                    return false;
                }

                outputfile.write((const char *)&currentBlockSize , sizeof(currentBlockSize));
                outputfile.write((const char *)&outlen , sizeof(outlen));
                outputfile.write((const char *)out.data() , outlen);
            }
        }
        listOfFiles.append("- Done\n");
        return true;
    }
    else{
        QMessageBox msgBox;
        msgBox.setText("Cannot open " + targetFile);
        msgBox.exec();
        return true;
    }
}

void MainWindow::butttonOutputDir_click(){
    const QString outputDir = QFileDialog::getExistingDirectory(0, ("Select Output Folder"), settings.value(ui->profileCombo->currentText() + "/outputpath", QDir::currentPath()).toString());
    if (outputDir.length()){
        ui->buttonOutputDir->setText(outputDir);
        settings.setValue(ui->profileCombo->currentText() + "/outputpath", ui->buttonOutputDir->text());
    }
}

void MainWindow::comboConfig_change(){
    settings.setValue("current-config-idx", ui->profileCombo->currentIndex());
    qDebug() << "Config index " << ui->profileCombo->currentIndex() << "\n";
    qDebug() << "In cache Config index " << settings.value("current-config-idx", -1) << "\n";
    restaureCurrentConfig();
}

void MainWindow::configAdd_click(){
    bool ok;
    const QString text = QInputDialog::getText(this, tr("New config name"),
                                               tr("Name:"), QLineEdit::Normal,
                                               QString(), &ok);
    if (ok && !text.isEmpty()){
        bool alreadyExist = false;
        for(int idx = 0 ; idx < ui->profileCombo->count() && !alreadyExist ; ++idx){
            alreadyExist |= text == ui->profileCombo->itemText(idx);
        }
        if(!alreadyExist){
            saveCurrentConfigDetails();
            ui->profileCombo->addItem(text);
            ui->profileCombo->setCurrentIndex(ui->profileCombo->count()-1);

            QStringList allConfigs;
            for(int idx = 0 ; idx < ui->profileCombo->count() ; ++idx){
                allConfigs << ui->profileCombo->itemText(idx);
            }
            settings.setValue("all-config", QVariant::fromValue(allConfigs));
            settings.setValue(ui->profileCombo->currentText() + "/outputpath", ui->buttonOutputDir->text());
            settings.setValue(ui->profileCombo->currentText() + "/keyLoad", ui->pubkeyPath->text());
            saveCurrentConfigDetails();
        }
        else{
            QMessageBox msgBox;
            msgBox.setText("Name \"" + text + "\" already exist.");
            msgBox.exec();
        }
    }
}

void MainWindow::configDel_click(){
    if(ui->profileCombo->count()>1){
        settings.remove(ui->profileCombo->currentText());
        ui->profileCombo->removeItem(ui->profileCombo->currentIndex());

        QStringList allConfigs;
        for(int idx = 0 ; idx < ui->profileCombo->count() ; ++idx){
            allConfigs << ui->profileCombo->itemText(idx);
        }
        settings.setValue("all-config", QVariant::fromValue(allConfigs));
    }
}

void MainWindow::buttonLoadKey_click(){
    const QString fileName = QFileDialog::getOpenFileName(this,
                                                          tr("Open key"), settings.value(ui->profileCombo->currentText() + "/key-path", QDir::homePath()).toString(), tr("Any Files (*.*)"));
    if(fileName.length()){
        if(!fileName.endsWith(".pub")){
            QMessageBox msgBox;
            msgBox.setText("The key loaded from " + fileName + " does not end with .pub.\n Please make sure it is correct.");
            msgBox.exec();
        }

        ui->pubkeyPath->setText(fileName);
        settings.setValue("key-path", fileName);
        settings.setValue(ui->profileCombo->currentText() + "/keyLoad", ui->pubkeyPath->text());
    }
}

void MainWindow::pubkeyPath_edited(){
    if(ui->pubkeyPath->text().size() != 0 && !QFile(ui->pubkeyPath->text()).exists()){
        QMessageBox msgBox;
        msgBox.setText("File " + ui->pubkeyPath->text() + " does not exist.");
        msgBox.exec();
    }
    else{
        settings.setValue("key-path", ui->pubkeyPath->text());
        settings.setValue(ui->profileCombo->currentText() + "/keyLoad", ui->pubkeyPath->text());
    }
}

void MainWindow::saveCurrentConfigDetails(){
    settings.setValue(ui->profileCombo->currentText() + "/checkPictures", ui->checkPictures->isChecked());
    settings.setValue(ui->profileCombo->currentText() + "/picturesFreq", ui->picturesFreq->value());
    settings.setValue(ui->profileCombo->currentText() + "/checkChanges", ui->checkChanges->isChecked());
    settings.setValue(ui->profileCombo->currentText() + "/changesPerct", ui->changesPerct->value());
    settings.setValue(ui->profileCombo->currentText() + "/checkCompact", ui->checkCompact->isChecked());
}

void MainWindow::restaureCurrentConfig(){
    ui->pubkeyPath->setText(settings.value(ui->profileCombo->currentText() + "/keyLoad", ui->pubkeyPath->text()).toString());
    ui->buttonOutputDir->setText(settings.value(ui->profileCombo->currentText() + "/outputpath", ui->buttonOutputDir->text()).toString());
    ui->checkPictures->setChecked(settings.value(ui->profileCombo->currentText() + "/checkPictures", ui->checkPictures->isChecked()).toBool());
    ui->picturesFreq->setValue(settings.value(ui->profileCombo->currentText() + "/picturesFreq", ui->picturesFreq->value()).toInt());
    ui->checkChanges->setChecked(settings.value(ui->profileCombo->currentText() + "/checkChanges", ui->checkChanges->isChecked()).toBool());
    ui->changesPerct->setValue(settings.value(ui->profileCombo->currentText() + "/changesPerct", ui->changesPerct->value()).toDouble());
    ui->checkCompact->setChecked(settings.value(ui->profileCombo->currentText() + "/checkCompact", ui->checkCompact->isChecked()).toBool());
}

void MainWindow::buttonSaveConfig_click(){
    const QString fileName = QFileDialog::getSaveFileName(this,
                                                          tr("Open config"), settings.value("config-path", QDir::homePath()).toString(), tr("Any Files (*.*)"));
    if(fileName.length()){
        qDebug() << "Save config " << fileName;
        settings.setValue("config-path", fileName);
        QSettings fileSetting(fileName, QSettings::IniFormat);
        fileSetting.value("configname", ui->profileCombo->currentText());
        fileSetting.setValue("/checkPictures", ui->checkPictures->isChecked());
        fileSetting.setValue("/picturesFreq", ui->picturesFreq->value());
        fileSetting.setValue("/checkChanges", ui->checkChanges->isChecked());
        fileSetting.setValue("/changesPerct", ui->changesPerct->value());
        fileSetting.setValue("/checkCompact", ui->checkCompact->isChecked());
        fileSetting.setValue("/outputpath", ui->buttonOutputDir->text());
        fileSetting.setValue("/keyLoad", ui->pubkeyPath->text());
    }
}

void MainWindow::buttonLoadConfig_click(){
    const QString fileName = QFileDialog::getOpenFileName(this,
                                                          tr("Save config"), settings.value("config-path", QDir::homePath()).toString(), tr("Any Files (*.*)"));
    if(fileName.length()){
        qDebug() << "Load config " << fileName;
        settings.setValue("config-path", fileName);

        QSettings fileSetting(fileName, QSettings::IniFormat);

        const QString text = fileSetting.value("configname", "unknown").toString();
        int foundIdx = -1;
        for(int idx = 0 ; idx < ui->profileCombo->count() && foundIdx == -1 ; ++idx){
            if(text == ui->profileCombo->itemText(idx)){
                foundIdx = idx;
            }
        }
        if(foundIdx == -1){
            ui->profileCombo->addItem(text);
            ui->profileCombo->setCurrentIndex(ui->profileCombo->count()-1);
        }
        else{
            ui->profileCombo->setCurrentIndex(foundIdx);
        }

        ui->pubkeyPath->setText(fileSetting.value("/keyLoad", ui->pubkeyPath->text()).toString());
        ui->buttonOutputDir->setText(fileSetting.value("/outputpath", ui->buttonOutputDir->text()).toString());
        ui->checkPictures->setChecked(fileSetting.value("/checkPictures", ui->checkPictures->isChecked()).toBool());
        ui->picturesFreq->setValue(fileSetting.value("/picturesFreq", ui->picturesFreq->value()).toInt());
        ui->checkChanges->setChecked(fileSetting.value("/checkChanges", ui->checkChanges->isChecked()).toBool());
        ui->changesPerct->setValue(fileSetting.value("/changesPerct", ui->changesPerct->value()).toDouble());
        ui->checkCompact->setChecked(fileSetting.value("/checkCompact", ui->checkCompact->isChecked()).toBool());

        saveCurrentConfigDetails();
    }
}
