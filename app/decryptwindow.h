#ifndef DECRYPTWINDOW_H
#define DECRYPTWINDOW_H

#include <QWidget>
#include <QSettings>

namespace Ui {
class DecryptWindow;
}

class DecryptWindow : public QWidget
{
    Q_OBJECT

public:
    explicit DecryptWindow(QWidget *parent = nullptr);
    ~DecryptWindow();

public slots:
    void buttonFolder_click();
    void buttonKey_click();
    void buttonDecrypt_click();

private:
    QSettings settings;
    Ui::DecryptWindow *ui;
};

#endif // DECRYPTWINDOW_H
