#include <QApplication>
#include <QLineEdit>
#include <QDir>
#include <QLineEdit>
#include <QFileDialog>
#include <QInputDialog>
#include <QSettings>
#include <QMessageBox>

#include <openssl/rsa.h>
#include <openssl/pem.h>
#include <openssl/err.h>

#include <iostream>

// https://riptutorial.com/openssl/example/16737/generate-rsa-key

int main(int argc, char *argv[])
{
    if((argc == 2 && QString(argv[1]) == "--help") || argc > 2){
        std::cout << "[HELP] Either you pass the target key file:\n";
        std::cout << "[HELP] $" << argv[0] << " [key file]\n";
        std::cout << "[HELP] Or you do not pass any parameter and use the Gui.\n";
        std::cout << "[HELP] If the selected file is 'X', the public key will be 'X.pub'.\n";
        return -1;
    }

    QApplication a(argc, argv);

    QSettings settings("UNISTRA", "RECME");
    QString fileName;
    if(argc == 2){
        fileName = argv[1];
    }
    else{
        fileName  = QFileDialog::getSaveFileName(nullptr,
            a.tr("New key"), settings.value("keygen/key-path", QDir::homePath()).toString(), a.tr("Any Files (*)"));
    }

    if(fileName.length()){
        std::cout << "[INFO] Output file will be " << fileName.toStdString() << "[.pub]" << std::endl;

        settings.setValue("keygen/key-path", QFileInfo(fileName).absolutePath());

        EVP_PKEY *pkey;
        pkey = EVP_PKEY_new();

        BIGNUM *bn;
        bn = BN_new();
        BN_set_word(bn, RSA_F4);

        RSA *rsa;
        rsa = RSA_new();
        RSA_generate_key_ex(
            rsa,  /* pointer to the RSA structure */
            2048, /* number of bits for the key - 2048 is a good value */
            bn,   /* exponent allocated earlier */
            NULL /* callback - can be NULL if progress isn't needed */
        );

        FILE  *fp  = NULL;

        fp = fopen(fileName.toStdString().c_str(), "w");
        if(fp == nullptr){
            QMessageBox msgBox;
            msgBox.setText("Cannot create " + fileName + ".");
            msgBox.exec();
            return -1;
        }
        if(PEM_write_RSAPrivateKey(fp, rsa, NULL, NULL, NULL, NULL, NULL) != 1){
            unsigned long err = ERR_get_error(); //Get the error number
            char err_msg[1024] = { 0 };
            ERR_error_string(err, err_msg);
            QMessageBox msgBox;
            msgBox.setText("Error in creating private key " + QString(err_msg) + " " + QString::number(err) + ".");
            msgBox.exec();
        }
        fclose(fp);

        fp = fopen((fileName + ".pub").toStdString().c_str(), "w");
        if(fp == nullptr){
            QMessageBox msgBox;
            msgBox.setText("Cannot create " + fileName + ".pub.");
            msgBox.exec();
            return -1;
        }
        if(PEM_write_RSAPublicKey(fp, rsa) != 1){
            unsigned long err = ERR_get_error();
            char err_msg[1024] = { 0 };
            ERR_error_string(err, err_msg);
            QMessageBox msgBox;
            msgBox.setText("Error in creating public key " + QString(err_msg) + " " + QString::number(err) + ".");
            msgBox.exec();
        }
        fclose(fp);

        EVP_PKEY_assign_RSA(pkey, rsa);
        EVP_PKEY_free(pkey);
    }
    else{
        std::cout << "[INFO] Empty filename, exit..." << std::endl;
    }

    return 0;
}

