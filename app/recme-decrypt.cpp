#include "decryptwindow.h"
#include "foldercompressor.hpp"

#include <QApplication>


#include <QFileDialog>
#include <QDir>
#include <QFile>
#include <QMessageBox>
#include <QTextStream>
#include <QDesktopWidget>
#include <QScreen>
#include <QDirIterator>
#include <QDebug>

#include <fstream>
#include <iostream>

#include <openssl/rsa.h>
#include <openssl/pem.h>
#include <openssl/err.h>

bool DecryptInConsole(const QString& inDirName, const QString& inKeyPath){
    if(!QDir(inDirName).exists() || !QFile(inDirName).exists()){
        std::cerr << "Path " << inDirName.toStdString() << " does not exist." << std::endl;
        return false;
    }
    if(inKeyPath.size() && !QFile(inKeyPath).exists()){
        std::cerr << "Key " << inKeyPath.toStdString() << " does not exist." << std::endl;
        return false;
    }


    EVP_PKEY *public_evp_key = NULL;
    EVP_PKEY_CTX *ctx = NULL;

    auto cleanPtr = [&](){
        if (public_evp_key != NULL)
            EVP_PKEY_free(public_evp_key);
        if (ctx != NULL)
            EVP_PKEY_CTX_free(ctx);
    };

    auto printError = [&](const char* label){
        unsigned long err = ERR_get_error(); //Get the error number
        char err_msg[1024] = { 0 };
        ERR_error_string(err, err_msg);
        std::cout << "Error: "<< label << " failed: " << err_msg << " " << err << std::endl;
        cleanPtr();
    };

    if(inKeyPath.size()){
        FILE *f;
        f = fopen(inKeyPath.toStdString().c_str(), "rb");
        if(f == nullptr){
            std::cout << "Cannot open " << inKeyPath.toStdString().c_str() << std::endl;
            return false;
        }

        public_evp_key = EVP_PKEY_new();
        if( PEM_read_PrivateKey(
            f,     /* use the FILE* that was opened */
            &public_evp_key, /* pointer to EVP_PKEY structure */
            NULL,  /* password callback - can be NULL */
            NULL   /* parameter passed to callback or password if callback is NULL */
                    ) == nullptr){
            unsigned long err = ERR_get_error(); //Get the error number
            char err_msg[1024] = { 0 };
            ERR_error_string(err, err_msg);
            std::cout << "Cannot load key " << err_msg << " " << err << "." << std::endl;
            fclose(f);
            return false;
        }
        fclose(f);

        ctx = EVP_PKEY_CTX_new(public_evp_key, NULL);
        if (ctx == NULL) {
            printError("EVP_PKEY_CTX_new");
            return false;
        }
        if (EVP_PKEY_decrypt_init(ctx) < 0) {
            printError("EVP_PKEY_decrypt_init");
            return false;
        }
        if (EVP_PKEY_CTX_set_rsa_padding(ctx, RSA_PKCS1_OAEP_PADDING/*EVP_PADDING_PKCS7*/) != 1) {
            printError("EVP_PKEY_CTX_set_rsa_padding");
            return false;
        }
    }

    // Uncompress folders
    {
        QStringList itcp;
        const QString extension("*.comp");
        if(QDir(inDirName).exists()){
            std::cout << "[INFO] Process dir " << inDirName.toStdString() << std::endl;
            QDirIterator it(inDirName, QStringList() << extension, QDir::Files, QDirIterator::Subdirectories);

            while (it.hasNext()){
                itcp << it.next();
            }
        }
        else if(QFile(inDirName).exists()){
            std::cout << "[INFO] Process file " << inDirName.toStdString() << std::endl;
            itcp << inDirName;
        }
        else{
            std::cout << "[ERROR] Invalid: " << inDirName.toStdString() << std::endl;
            cleanPtr();
            return false;
        }

        int counter = 0;
        for(const QString& sourceFile : itcp){
            std::cout << "[INFO] " << 100*(counter++/double(itcp.size())) << std::endl;

            QString targetFile = sourceFile.left(sourceFile.length() - extension.length() + 1) + "-decomp";
            std::cout << "[INFO] Decompress: " << sourceFile.toStdString() << " to " << targetFile.toStdString() << std::endl;

            if(QDir(targetFile).exists()){
                std::cout << "[INFO] Already exist..." << std::endl;
            }
            else if(FolderCompressor().decompressFolder(sourceFile, targetFile) == false){
                std::cerr << "Failed to uncompress " << sourceFile.toStdString() << std::endl;
                return  false;
            }
        }
    }
    // Uncryte files
    {
        QStringList itcp;
        const QString extension("*.recme");
        if(QDir(inDirName).exists()){
            std::cout << "[INFO] Process dir " << inDirName.toStdString() << std::endl;
            QDirIterator it(inDirName, QStringList() << extension, QDir::Files, QDirIterator::Subdirectories);

            while (it.hasNext()){
                itcp << it.next();
            }
        }
        else if(QFile(inDirName).exists()){
            std::cout << "[INFO] Process file " << inDirName.toStdString() << std::endl;
            itcp << inDirName;
        }
        else{
            std::cout << "[ERROR] Invalid: " << inDirName.toStdString() << std::endl;
            cleanPtr();
            return false;
        }

        std::vector<unsigned char> content;
        std::vector<unsigned char> out;

        int counter = 0;
        for(const QString& sourceFile : itcp){
            std::cout << "[INFO] " << 100*(counter++/double(itcp.size())) << std::endl;

            QString targetFile = sourceFile.left(sourceFile.length() - extension.length() + 1);
            std::cout << "[INFO] Decrypt: " << sourceFile.toStdString() << " to " << targetFile.toStdString() << std::endl;

            if(QFile(targetFile).exists()){
                std::cout << "[INFO] Already exist..." << std::endl;
            }
            else{
                // binary mode is only for switching off newline translation
                std::ifstream file(sourceFile.toStdString(), std::ios::binary);
                if(!file.is_open()){
                    std::cerr << "Cannot open file " << sourceFile.toStdString() << std::endl;
                }
                else{
                    file.unsetf(std::ios::skipws);

                    std::streampos file_size;
                    file.seekg(0, std::ios::end);
                    file_size = file.tellg();
                    file.seekg(0, std::ios::beg);
                    content.reserve(file_size);
                    content.resize(0);

                    content.insert(content.begin(),
                               std::istream_iterator<unsigned char>(file),
                               std::istream_iterator<unsigned char>());

                    std::ofstream outputfile(targetFile.toStdString(), std::ios::binary);

                    if(content.size()){
                        size_t idxData = 0;
                        while( idxData < content.size()){
                            if(idxData+sizeof(int) > content.size()){
                                std::cerr << "Cannot read next block size at " << idxData << " " << sourceFile.toStdString() << std::endl;
                                break;
                            }

                            const size_t sizeBlockOriginal = *(size_t*)&content[idxData];
                            idxData += sizeof(size_t);
                            const int sizeBlockCrypted = *(size_t*)&content[idxData];
                            idxData += sizeof(size_t);

                            if(idxData+sizeBlockCrypted > content.size()){
                                std::cerr << "Cannot read next block " << idxData << " " << sourceFile.toStdString() << std::endl;
                                break;
                            }

                            size_t outlen = 0;
                            if (EVP_PKEY_decrypt(ctx, NULL, &outlen, &content[idxData], sizeBlockOriginal) <= 0){
                                printError("EVP_PKEY_decrypt - get size ");
                                return false;
                            }

                            out.resize(outlen);

                            if (EVP_PKEY_decrypt(ctx, out.data(), &outlen, &content[idxData], sizeBlockCrypted) <= 0){
                                printError("EVP_PKEY_decrypt - decrypt ");
                                return false;
                            }
                            if(outlen != sizeBlockOriginal){
                                std::cerr << "Size missmatch " << outlen << " " << sizeBlockOriginal << std::endl;
                                break;
                            }

                            idxData += sizeBlockCrypted;

                            outputfile.write((const char *)out.data() , outlen);
                        }
                    }
                }
            }
        }
    }

    cleanPtr();
    return true;
}


int main(int argc, char *argv[])
{
    if(argc == 1){
        QApplication a(argc, argv);
        DecryptWindow w;
        w.show();
        return a.exec();
    }

    if(argc != 3){
        std::cerr << "Error you must pass a directory/file to decrypt and a key:\n";
        std::cerr << argv[0] << " [DIR/FILE] [SSH KEY]" << std::endl;
        std::cerr << std::endl;
        std::cerr << "No argument will open the GUI" << std::endl;
    }

    return DecryptInConsole(argv[1], argv[2]) == true;
}

