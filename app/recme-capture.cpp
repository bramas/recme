#include "mainwindow.h"

#include <QApplication>


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    if(argc == 2 && QString("--run") == argv[1]){
        QApplication::processEvents();
        w.startFromCm();
        QApplication::processEvents();
    }
    return a.exec();
}

