#include "decryptwindow.h"
#include "ui_decryptwindow.h"
#include "foldercompressor.hpp"

#include <QFileDialog>
#include <QDir>
#include <QFile>
#include <QMessageBox>
#include <QTextStream>
#include <QDesktopWidget>
#include <QScreen>
#include <QDirIterator>
#include <QDebug>

#include <fstream>
#include <iostream>

#include <openssl/rsa.h>
#include <openssl/pem.h>
#include <openssl/err.h>

DecryptWindow::DecryptWindow(QWidget *parent) :
    QWidget(parent), settings("UNISTRA", "RECME"),
    ui(new Ui::DecryptWindow)
{
    ui->setupUi(this);

    QDesktopWidget* widget = QApplication::desktop();//qApp->desktop();
    QRect screenSize = widget->availableGeometry(widget->primaryScreen());
    this->setGeometry(screenSize.width()/2-width()/2, screenSize.height()/2-height()/2, width(), height());
    this->setMinimumSize(QSize(width(),height()));
    this->setMaximumSize(QSize(width()*1.5,height()*1.5));

    ui->buttonFolder->setText(settings.value("decrypt/buttonFolder", ui->buttonFolder->text()).toString());
    ui->buttonLoadKey->setText(settings.value("decrypt/buttonLoadKey", ui->buttonLoadKey->text()).toString());
}

DecryptWindow::~DecryptWindow()
{
    delete ui;
}

void DecryptWindow::buttonFolder_click(){
    const QString outputDir = QFileDialog::getExistingDirectory(0, ("Select Folder to decrypt"), settings.value("decrypt/buttonFolder", QDir::currentPath()).toString());
    if (outputDir.length()){
        ui->buttonFolder->setText(outputDir);
        settings.setValue("decrypt/buttonFolder", ui->buttonFolder->text());
    }
}

void DecryptWindow::buttonKey_click(){
    const QString fileName = QFileDialog::getOpenFileName(this,
                                                          tr("Open key"), settings.value("decrypt/keypath", QDir::homePath()).toString(), tr("Any Files (*)"));
    if(fileName.length()){
        if(fileName.endsWith(".pub")){
            QMessageBox msgBox;
            msgBox.setText("The key loaded from " + fileName + " seems to be a public key.\n Please make sure it is correct.");
            msgBox.exec();
        }

        settings.setValue("decrypt/keypath", QFileInfo(fileName).absolutePath());
        ui->buttonLoadKey->setText(fileName);
        settings.setValue("decrypt/buttonLoadKey", fileName);
    }
}


// https://www.programmersought.com/article/37955188510/
// https://www.openssl.org/docs/man1.0.2/man3/EVP_PKEY_decrypt.html
void DecryptWindow::buttonDecrypt_click(){    
    if(!QDir(ui->buttonFolder->text()).exists()){
        QMessageBox msgBox;
        msgBox.setText("Directory " + ui->buttonFolder->text() + " does not exist.");
        msgBox.exec();
        return;
    }
    if(!QFile(ui->buttonLoadKey->text()).exists()){
        QMessageBox msgBox;
        msgBox.setText("File " + ui->buttonLoadKey->text() + " does not exist.");
        msgBox.exec();
        return;
    }

    FILE *f;
    f = fopen(ui->buttonLoadKey->text().toStdString().c_str(), "rb");
    if(f == nullptr){
        QMessageBox msgBox;
        msgBox.setText("Cannot open key file " + ui->buttonLoadKey->text() + " .");
        msgBox.exec();
        return;
    }
    EVP_PKEY *public_evp_key;
    public_evp_key = EVP_PKEY_new();
    if( PEM_read_PrivateKey(
        f,     /* use the FILE* that was opened */
        &public_evp_key, /* pointer to EVP_PKEY structure */
        NULL,  /* password callback - can be NULL */
        NULL   /* parameter passed to callback or password if callback is NULL */
                ) == nullptr){
        unsigned long err = ERR_get_error(); //Get the error number
        char err_msg[1024] = { 0 };
        ERR_error_string(err, err_msg);
        QMessageBox msgBox;
        msgBox.setText("Cannot load key " + QString(err_msg) + " " + QString::number(err) + ".");
        msgBox.exec();
        fclose(f);
        return ;
    }
    fclose(f);

    EVP_PKEY_CTX *ctx = NULL;

    auto cleanPtr = [&](){
        if (public_evp_key != NULL)
            EVP_PKEY_free(public_evp_key);
        if (ctx != NULL)
            EVP_PKEY_CTX_free(ctx);
    };

    auto printError = [&](const char* label){
        unsigned long err = ERR_get_error(); //Get the error number
        char err_msg[1024] = { 0 };
        ERR_error_string(err, err_msg);
        QMessageBox msgBox;
        msgBox.setText(QString(label) + " failed: " + QString(err_msg) + " " + QString::number(err) + ".");
        msgBox.exec();
        cleanPtr();
    };

    ctx = EVP_PKEY_CTX_new(public_evp_key, NULL);
    if (ctx == NULL) {
        printError("EVP_PKEY_CTX_new");
        return;
    }
    if (EVP_PKEY_decrypt_init(ctx) < 0) {
        printError("EVP_PKEY_decrypt_init");
        return;
    }
    if (EVP_PKEY_CTX_set_rsa_padding(ctx, RSA_PKCS1_OAEP_PADDING/*EVP_PADDING_PKCS7*/) != 1) {
        printError("EVP_PKEY_CTX_set_rsa_padding");
        return;
    }

    this->setEnabled(false);
    QApplication::processEvents();

    // Uncompress folders
    {
        std::cout << "[INFO] Process dir " << ui->buttonFolder->text().toStdString() << std::endl;
        const QString extension("*.comp");
        QDirIterator it(ui->buttonFolder->text(), QStringList() << extension, QDir::Files, QDirIterator::Subdirectories);

        QStringList itcp;
        while (it.hasNext()){
            itcp << it.next();
        }

        ui->progressBar->setMaximum(itcp.size());
        for(const QString& sourceFile : itcp){
            ui->progressBar->setValue(ui->progressBar->value()+1);
            ui->progressBar->setFormat(sourceFile + " (%p)");
            QApplication::processEvents();

            QString targetFile = sourceFile.left(sourceFile.length() - extension.length() + 1) + "-decomp";
            std::cout << "[INFO] Decompress: " << sourceFile.toStdString() << " to " << targetFile.toStdString() << std::endl;

            if(QDir(targetFile).exists()){
                std::cout << "[INFO] Already exist..." << std::endl;
            }
            else if(FolderCompressor().decompressFolder(sourceFile, targetFile) == false){
                QMessageBox msgBox;
                msgBox.setText("Failed to uncompress " + sourceFile);
                msgBox.exec();
            }
        }
    }
    // Uncryte files
    {
        std::cout << "[INFO] Process dir " << ui->buttonFolder->text().toStdString() << std::endl;
        const QString extension("*.recme");
        QDirIterator it(ui->buttonFolder->text(), QStringList() << extension, QDir::Files, QDirIterator::Subdirectories);

        QStringList itcp;
        while (it.hasNext()){
            itcp << it.next();
        }

        std::vector<unsigned char> content;
        std::vector<unsigned char> out;

        ui->progressBar->setMaximum(itcp.size());
        for(const QString& sourceFile : itcp){
            ui->progressBar->setValue(ui->progressBar->value()+1);
            ui->progressBar->setFormat(sourceFile + " (%p)");
            QApplication::processEvents();

            QString targetFile = sourceFile.left(sourceFile.length() - extension.length() + 1);
            std::cout << "[INFO] Decrypt: " << sourceFile.toStdString() << " to " << targetFile.toStdString() << std::endl;

            if(QFile(targetFile).exists()){
                std::cout << "[INFO] Already exist..." << std::endl;
            }
            else{
                // binary mode is only for switching off newline translation
                std::ifstream file(sourceFile.toStdString(), std::ios::binary);
                if(!file.is_open()){
                    QMessageBox msgBox;
                    msgBox.setText("Cannot open file " + sourceFile + " .");
                    msgBox.exec();
                }
                else{
                    file.unsetf(std::ios::skipws);

                    std::streampos file_size;
                    file.seekg(0, std::ios::end);
                    file_size = file.tellg();
                    file.seekg(0, std::ios::beg);
                    content.reserve(file_size);
                    content.resize(0);

                    content.insert(content.begin(),
                               std::istream_iterator<unsigned char>(file),
                               std::istream_iterator<unsigned char>());

                    std::ofstream outputfile(targetFile.toStdString(), std::ios::binary);

                    if(content.size()){
                        size_t idxData = 0;
                        while( idxData < content.size()){
                            if(idxData+sizeof(int) > content.size()){
                                QMessageBox msgBox;
                                msgBox.setText("Cannot read next block size at " + QString::number(idxData) +  " " + sourceFile + " .");
                                msgBox.exec();
                                break;
                            }

                            const size_t sizeBlockOriginal = *(size_t*)&content[idxData];
                            idxData += sizeof(size_t);
                            const int sizeBlockCrypted = *(size_t*)&content[idxData];
                            idxData += sizeof(size_t);

                            if(idxData+sizeBlockCrypted > content.size()){
                                QMessageBox msgBox;
                                msgBox.setText("Cannot read next block " + QString::number(idxData) +  " " + sourceFile + " .");
                                msgBox.exec();
                                break;
                            }

                            size_t outlen = 0;
                            if (EVP_PKEY_decrypt(ctx, NULL, &outlen, &content[idxData], sizeBlockOriginal) <= 0){
                                printError("EVP_PKEY_decrypt - get size ");
                                this->setEnabled(true);
                                return;
                            }

                            out.resize(outlen);

                            if (EVP_PKEY_decrypt(ctx, out.data(), &outlen, &content[idxData], sizeBlockCrypted) <= 0){
                                printError("EVP_PKEY_decrypt - decrypt ");
                                this->setEnabled(true);
                                return;
                            }
                            if(outlen != sizeBlockOriginal){
                                QMessageBox msgBox;
                                msgBox.setText("Size missmatch " + QString::number(outlen) +  " " + QString::number(sizeBlockOriginal) + " .");
                                msgBox.exec();
                                break;
                            }

                            idxData += sizeBlockCrypted;

                            outputfile.write((const char *)out.data() , outlen);
                        }
                    }
                }
            }
        }
    }

    // Done
    ui->progressBar->setValue(0);
    ui->progressBar->setFormat("");

    this->setEnabled(true);
    cleanPtr();
}


