RECME

# About

RECME is a software that automatically takes screenshots and crypts them using RSA public key.
It also has two other binaries, one to generate a key pair, and another to decrypt the screenshots.
Its aim is to capture the activity of someone (like a student) when he/she is in a situation that requires a control (like a practical work with a mark).
The software cannot be used to spy, as there is no automatic start and there is a visible button when it is running.

# Needs

RECME needs:
- C++ compiler
- CMake
- Qt5 dev libs
- OpenSSL

For example, with Ubuntu, one can install: `sudo apt-get install build-essential cmake qtbase5-dev qt5-default qtbase5-private-dev openssl libssl-dev`

# Building

```
# go in the recme dir:
cd recme
# create a build dir
mkdir build && cd build
# run cmake
cmake ../app
# make
make
```

# Binaries

- recme-capture: to capture the screen
- recme-decrypt: to decrypt what has been captured
- recme-keygen: to generate keys

# Getting some help
Open an issue here or contact Berenger (berenger.bramas@inria.fr)
